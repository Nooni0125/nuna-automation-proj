package page_package;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.openqa.selenium.support.How.CSS;

public class BasePage extends ObjectPage{


    @FindBy(linkText = "Sign in")
    private WebElement signInButton;

    @FindBy(linkText = "Contact us")
    private WebElement contactUsButton;

    @FindBy(css = ".icon-phone~strong")
    private WebElement contactNumber;

    @FindBy(id = "search_query_top")
    private WebElement searchTextbox;

    @FindBy(name = "submit_search")
    private WebElement searchButton;

    @FindBy(css = "[title='View my shopping cart']")
    private WebElement shopingCArtLink;


    public BasePage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    public String getPhoneNumber(){
        return this.contactNumber.getText();
    }


}
