package page_package;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.SkipException;

public class ObjectPage {
    // Variables definition
     protected WebDriver driver;
    protected WebDriverWait wait;

    public ObjectPage(WebDriver driver, WebDriverWait wait) {

        this.driver = driver;
        this.wait = wait;
        PageFactory.initElements(driver, this);
    }

    public void click(WebElement element) {
        element.click();
    }

    public void type(WebElement element ,String input) {
        element.sendKeys(input);
    }

    public void waitUntilVisibilityOfElementLocated(String locator, int  locatorCode) {
        // Constant Definition (locator type code)
        final int ID = 1;
        final int NAME = 2;
        final int CLASS_NAME = 3;
        final int TAG_NAAME = 4;
        final int LINK_TEXT = 5;
        final int PARTIAL_LINK_TEXT = 6;
        final int CSS_SELECTOR = 7;
        final int XPATH = 8;

        switch(locatorCode) {
            case(ID): {
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(locator)));
                break;
            }
            case(NAME): {
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(locator)));
                break;
            }
            case(CLASS_NAME): {
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(locator)));
                break;
            }
            case(TAG_NAAME): {
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.tagName(locator)));
                break;
            }
            case(LINK_TEXT): {
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText(locator)));
                break;
            }
            case(PARTIAL_LINK_TEXT): {
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.partialLinkText(locator)));
                break;
            }
            case(CSS_SELECTOR): {
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(locator)));
                break;
            }
            case(XPATH): {
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)));
                break;
            }

            default:{
                System.out.println("wrong code eror");
                throw new SkipException("wrong code");
            }

        }
    }

    public String getText(WebElement element) {
        return element.getText();
    }


}
