package test_package;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import page_package.BasePage;
import  page_package.ObjectPage;

public class BaseTest {
    {
        System.setProperty("webdriver.chrome.driver", "C:\\selenium_JARS\\chromedriver.exe");
    }


    public static WebDriver driver;
    public static WebDriverWait wait;

    @BeforeTest
    public static void  setUp() {
        // Constant definition
        final String PATH = "http://automationpractice.com/index.php";

        // Variable definition
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 10);

        driver.get(PATH);
        driver.manage().window().maximize();

        BasePage basepage = new BasePage(driver, wait);
        final int CSS_SELECTOR = 7;


        basepage.waitUntilVisibilityOfElementLocated(".icon-phone~strong", CSS_SELECTOR);
        System.out.println("located");
        System.out.println(basepage.getPhoneNumber());

    }

    @AfterTest
    public static void tearDown(){
        //nothing here yet
    }
}
